export const kTc = (kelvin) => {
  return parseFloat(kelvin-273.15, 10).toFixed(2);
}