import { Fragment, useState, useEffect } from 'react';

import Header from './components/Header';
import Formulario from './components/Formulario';
import Clima from './components/Clima';
import Error from './components/Error';

function App() {

  // State
  const [ informacion, actualizarInformacion ] = useState({
    ciudad: '',
    pais: ''
  });
  const [ consular, actualizarConsultar ] = useState(false);
  const [ data, actualizarData ] = useState({});
  const [ error, actualizarError ] = useState(false);
  
  const { ciudad, pais } = informacion;

  useEffect(() => {
    if (consular) {
      consultar();
    }
    // eslint-disable-next-line
  }, [consular]);

  const consultar = async () => {
    const key = 'a8f8d50d111685e45d44fcf54f9f1f13';
    const URI = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${key}`;

    actualizarData({});

    try {
      const respuesta = await fetch(URI);
      const resultado = await respuesta.json();
      if (resultado.cod === 200) {
        actualizarData({
          name: resultado.name,
          main: resultado.main
        });
        actualizarError(false);
      } else if (resultado.cod === '404') {
        actualizarError(true);
      }
      actualizarConsultar(false);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <Fragment>
      <Header titulo="Clima" />
      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Formulario
                informacion={ informacion }
                actualizarInformacion={ actualizarInformacion }
                actualizarConsultar={ actualizarConsultar }
              />
            </div>
            <div className="col m6 s12">
              {
                error 
                  ?
                <Error mensaje="Ciudad no encontrada" />
                  :
                <Clima data={data} />
              }
              
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
