import { Fragment, useState } from 'react';
import PropTypes from 'prop-types';

import Error from './Error';

const Formulario = ({ informacion, actualizarInformacion, actualizarConsultar }) => {

  // State para el formulario

  const [ error, actualizarError ] = useState(false);

  const { ciudad, pais } = informacion;

  // Funcion que agrega los valores en el state
  const handleChange = e => {
    actualizarInformacion({
      ...informacion,
      [e.target.name]: e.target.value
    })
  }

  //Validar Submit
  const handleSubmit = e => {
    e.preventDefault();
    if (ciudad.trim() === '' || pais.trim() === '') {
      actualizarError(true);
      return;
    }
    actualizarError(false);
    actualizarConsultar(true);
  }

  return (
    <Fragment>
      {
        error
          ?
        <Error mensaje="Todos los campos son obligatorios" />
          :
        null
      }
      <form onSubmit={ handleSubmit }>
        <div className="input-field col s12">
          <input
            type="text"
            name="ciudad"
            id="ciudad"
            value={ ciudad }
            onChange={ handleChange }
            autoComplete="off"
          />
          <label htmlFor="ciudad">Ciudad</label>
        </div>
        <div className="input-field col s12">
          <select
            name="pais"
            id="pais"
            value={ pais }
            onChange={ handleChange }
            autoComplete="off"
          >
            <option value="" disabled>Seleciona un país</option>
            <option value="US">Estados Unidos</option>
            <option value="MX">México</option>
            <option value="AR">Argentina</option>
            <option value="CO">Colombia</option>
            <option value="CR">Costa Rica</option>
            <option value="ES">España</option>
            <option value="PE">Perú</option>
          </select>
          <label htmlFor="pais">Ciudad</label>
        </div>
        <div className="input-field col s12">
          <button
            className="waves-effect waves-light btn-large btn-block yellow accent-4  col s12"
            type="submit"
          >
            Consultar
          </button>
        </div>
      </form>
    </Fragment>
  );
};

Formulario.propTypes = {
  informacion: PropTypes.object.isRequired,
  actualizarInformacion: PropTypes.func.isRequired,
  actualizarConsultar: PropTypes.func.isRequired
};

export default Formulario;