import React from 'react';
import PropTypes from 'prop-types';

import { kTc } from '../helper/temperatura';

const Clima = ({ data }) => {

  const { name, main } = data;

  if (!name || !main) return null;
  //if (Object.keys(data)) return null;
  
  //const { temp, feels_like, temp_max, temp_min } = main;

  return (
    <div className="card-panel white-text col s12">
      <div className="black-text">
        <h2>{ name }</h2>
        <p className="temperatura">
          { kTc(main.temp) }<span>&#x2103;</span>
        </p>
        <p>
          <b>Sensación de</b> { kTc(main.feels_like) }<span>&#x2103;</span>
        </p>
        <p>
          <b>Maxima: </b>{ kTc(main.temp_max) }<span>&#x2103;</span>
        </p>
        <p>
          <b>Minima: </b>{ kTc(main.temp_min) }<span>&#x2103;</span>
        </p>
      </div>
    </div>
  );

};

Clima.propTypes = {
  data: PropTypes.object.isRequired
};

export default Clima;